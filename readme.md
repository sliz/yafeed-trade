- 行业表
```sql
CREATE TABLE industry (
    industry_id BIGINT COMMENT '主键' NOT NULL PRIMARY KEY,
    industry_name VARCHAR(20) COMMENT '行业名称' NOT NULL DEFAULT '',
    industry_code VARCHAR(20) COMMENT '行业代码' NOT NULL DEFAULT '',
    companies INT COMMENT '公司数量' NOT NULL DEFAULT 0,
    average_price DECIMAL(20 , 10 ) COMMENT '平均价格' NOT NULL DEFAULT 0,
    change_amount DECIMAL(20 , 10 ) COMMENT '涨跌额' NOT NULL DEFAULT 0,
    change_range DECIMAL(20 , 10 ) COMMENT '涨跌幅' NOT NULL DEFAULT 0,
    trade_volume BIGINT COMMENT '成交量' NOT NULL DEFAULT 0,
    trade_price BIGINT COMMENT '成交额' NOT NULL DEFAULT 0,
    update_time TIMESTAMP COMMENT '更新时间' DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    create_time TIMESTAMP COMMENT '创建时间' DEFAULT CURRENT_TIMESTAMP
)
```
- 行业领涨股
```sql
CREATE TABLE `leader_stock` (
   `leader_stock_id` bigint NOT NULL COMMENT '主键',
   `industry_id` bigint DEFAULT NULL COMMENT '行业表主键',
   `industry_code` varchar(20) NOT NULL COMMENT '行业编码',
   `stock_code` varchar(20) NOT NULL COMMENT '股票代码',
   `stock_name` varchar(20) NOT NULL COMMENT '股票名称',
   `current_price` decimal(20,10) NOT NULL COMMENT '当前价',
   `change_range` decimal(20,10) NOT NULL COMMENT '涨跌幅',
   `change_amount` decimal(20,10) NOT NULL COMMENT '涨跌额',
   `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
   `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
   PRIMARY KEY (`leader_stock_id`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='行业领涨股'
```

- 行业个股
```sql
CREATE TABLE `industry_stock` (
   `industry_stock_id` bigint NOT NULL COMMENT '主键',
   `stock_code` varchar(20) NOT NULL DEFAULT '' COMMENT '股票代码',
   `stock_name` varchar(20) NOT NULL DEFAULT '' COMMENT '股票名称',
   `current_price` decimal(20,10) NOT NULL DEFAULT '0.0000000000' COMMENT '当前价',
   `pre_price` decimal(20,10) NOT NULL DEFAULT '0.0000000000' COMMENT '昨收',
   `change_amount` decimal(20,10) NOT NULL DEFAULT '0.0000000000' COMMENT '涨跌额',
   `change_range` decimal(20,10) NOT NULL DEFAULT '0.0000000000' COMMENT '涨跌幅',
   `high_price` decimal(20,10) NOT NULL DEFAULT '0.0000000000' COMMENT '最高价',
   `low_price` decimal(20,10) NOT NULL DEFAULT '0.0000000000' COMMENT '最低价',
   `trade_volume` bigint NOT NULL DEFAULT '0' COMMENT '交易量',
   `trade_price` bigint NOT NULL DEFAULT '0' COMMENT '交易额',
   `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
   `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
   PRIMARY KEY (`industry_stock_id`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='行业个股'
```
- 历史个股
```sql
CREATE TABLE history_stock (
    history_stock_id BIGINT COMMENT '主键' PRIMARY KEY,
    trade_day TIMESTAMP COMMENT '交易时间' NOT NULL,
    stock_code VARCHAR(20) COMMENT '股票代码' NOT NULL,
    stock_name VARCHAR(20) COMMENT '股票名称' NOT NULL,
    open_price DECIMAL(20 , 10 ) COMMENT '开盘价' NOT NULL,
    close_price DECIMAL(20 , 10 ) COMMENT '收盘价' NOT NULL,
    high_price DECIMAL(20 , 10 ) COMMENT '最高价' NOT NULL,
    low_price DECIMAL(20 , 10 ) COMMENT '最低价' NOT NULL,
    trade_volume BIGINT COMMENT '成交量' NOT NULL,
    ma20_price DECIMAL(20 , 10 ) COMMENT 'ma20 价' NOT NULL,
    ma20_volume BIGINT COMMENT 'ma20 成交量' NOT NULL,
    update_time TIMESTAMP COMMENT '更新时间' NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    create_time TIMESTAMP COMMENT '创建时间' NOT NULL DEFAULT CURRENT_TIMESTAMP
)  COMMENT='个股历史成交信息'
```