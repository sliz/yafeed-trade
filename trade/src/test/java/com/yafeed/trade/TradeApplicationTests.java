package com.yafeed.trade;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.robert.vesta.service.intf.IdService;
import com.yafeed.trade.dao.IndustryStockMapper;
import com.yafeed.trade.entity.industry.Industry;
import com.yafeed.trade.entity.industrystock.IndustryStock;
import com.yafeed.trade.pojo.dto.industry.IndustryDto;
import com.yafeed.trade.service.industry.IndustryService;
import com.yafeed.trade.service.stock.HistoryStockService;
import com.yafeed.trade.service.stock.IndustryStockService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

@SpringBootTest
class TradeApplicationTests {

    @Autowired
    private IndustryService industryService;

    @Autowired
    private IndustryStockService industryStockService;


    @Resource
    private IndustryStockMapper industryStockMapper;


    @Autowired
    private HistoryStockService historyStockService;

    @Test
    void updateHistoryStock() {
        int pageNo = 1;
        int pageSize = 100;
        PageInfo<IndustryStock> pageInfo = historyStockService.updateHistoryStock(pageNo, pageSize);
        try {
            while (pageNo < pageInfo.getPages()) {
                System.out.println("页码 ：" + pageNo);
                pageInfo = historyStockService.updateHistoryStock(++pageNo, pageSize);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(pageNo);
        }


    }


    void fetchSinaIndustries() {
        industryService.refreshIndustryIndividualStockFromSina();
    }


    void historyStockIndustry() {
        PageHelper.startPage(1, 200, "stock_code desc");
        List<IndustryStock> industryStocks = industryStockService.listDistinctIndustryStockSymbol();
        PageInfo<IndustryStock> pageInfo = new PageInfo<>(industryStocks);
        System.out.println(pageInfo.getTotal());

    }


    void listLastIndustry() {
        Industry industry = industryService.listLastIndustry();
        System.out.println(industry);
    }


    void fetchSinaIndustryStock() {
        List<IndustryDto> industryDtos = industryService.fetchSinaIndustries();
        int sum = industryStockService.fetchSinaIndustriesStock(industryDtos);
        System.out.println(sum);
    }
}
