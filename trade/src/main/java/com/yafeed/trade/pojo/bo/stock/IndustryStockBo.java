package com.yafeed.trade.pojo.bo.stock;

import cn.hutool.core.map.MapUtil;
import com.robert.vesta.service.intf.IdService;
import com.yafeed.trade.common.enums.DateTypeEnum;
import com.yafeed.trade.common.tuple.TwoTuple;
import com.yafeed.trade.entity.industrystock.IndustryStock;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @description: 行业个股
 * @author: yanghj
 * @time: 2020-12-18 11:02
 **/
public class IndustryStockBo {

    public static IndustryStockBo of() {
        return new IndustryStockBo();
    }


    private Map<String, IndustryStock> transferToIndustryStockMap(List<IndustryStock> stockList) {
        String format = DateTypeEnum.YYYY_MM_DD.now();
        return stockList.stream().collect(Collectors.toMap(m -> m.getStockCode() + "_" + format, item -> item));
    }

    public TwoTuple<Boolean, List<IndustryStock>> parseIndustryStock(List<IndustryStock> exists, List<IndustryStock> industryStocks) {
        TwoTuple<Boolean, List<IndustryStock>> twoTuple = new TwoTuple<>();
        twoTuple.setSecond(industryStocks);
        if (exists == null || exists.isEmpty()) {
            twoTuple.setFirst(false);
            return twoTuple;
        }
        Map<String, IndustryStock> industryStockMap = transferToIndustryStockMap(exists);

        Map<String, IndustryStock> industryStockDtoMap = transferToIndustryStockMap(industryStocks);

        for (Map.Entry<String, IndustryStock> entry : industryStockMap.entrySet()) {
            IndustryStock industryStockDto = industryStockDtoMap.get(entry.getKey());
            copyProperties(industryStockDto, entry.getValue());
        }
        twoTuple.setFirst(true);
        return twoTuple;
    }


    /**
     * <pre>
     *     属性拷贝
     * </pre>
     *
     * @param target 目标
     * @param source 源
     */
    private void copyProperties(IndustryStock target, IndustryStock source) {
        target.setIndustryStockId(source.getIndustryStockId());
    }

    private IndustryStock or() {
        return new IndustryStock();
    }


    public static IndustryStock or(Map<String, Object> map, IdService idService) {
        if (map.isEmpty()) {
            throw new RuntimeException("map 不能为空");
        }
        IndustryStock industryStock = of().or();
        industryStock.setIndustryStockId(idService.genId());
        industryStock.setStockCode(MapUtil.getStr(map, "symbol"));
        industryStock.setStockName(MapUtil.getStr(map, "name"));
        industryStock.setCurrentPrice(MapUtil.get(map, "trade", BigDecimal.class));
        industryStock.setChangeAmount(MapUtil.get(map, "pricechange", BigDecimal.class));
        industryStock.setChangeRange(MapUtil.get(map, "changepercent", BigDecimal.class));
        industryStock.setPrePrice(MapUtil.get(map, "settlement", BigDecimal.class));
        industryStock.setTradePrice(MapUtil.get(map, "volume", Long.class));
        industryStock.setTradeVolume(MapUtil.get(map, "amount", Long.class));
        industryStock.setHighPrice(MapUtil.get(map, "high", BigDecimal.class));
        industryStock.setLowPrice(MapUtil.get(map, "low", BigDecimal.class));
        return industryStock;
    }


}
