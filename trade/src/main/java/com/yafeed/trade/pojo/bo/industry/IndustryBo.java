package com.yafeed.trade.pojo.bo.industry;

import cn.hutool.core.map.MapUtil;
import com.yafeed.trade.common.enums.DateTypeEnum;
import com.yafeed.trade.common.tuple.TwoTuple;
import com.yafeed.trade.entity.industry.Industry;
import com.yafeed.trade.entity.leaderstock.LeaderStock;
import com.yafeed.trade.pojo.dto.industry.IndustryDto;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @description: 行业信息
 * @author: yanghj
 * @time: 2020-12-15 15:35
 **/
public class IndustryBo {


    public static IndustryBo of() {
        return new IndustryBo();
    }

    /**
     * <pre>
     *     行业信息中领涨股
     * </pre>
     *
     * @param industries 行业信息
     * @return list
     */
    public TwoTuple<Boolean, List<LeaderStock>> filterLeaderStock(TwoTuple<Boolean, List<IndustryDto>> industryTuple) {
        TwoTuple<Boolean, List<LeaderStock>> leaderTuple = new TwoTuple<>();
        leaderTuple.setFirst(industryTuple.getFirst());

        List<IndustryDto> industries = industryTuple.getSecond();
        if (industries == null || industries.isEmpty()) {
            leaderTuple.setSecond(new ArrayList<>());
            return leaderTuple;
        }

        List<LeaderStock> leaderStocks = industries.stream().map(IndustryDto::getLeaderStock).collect(Collectors.toList());
        leaderTuple.setSecond(leaderStocks);
        return leaderTuple;
    }


    /**
     * <pre>
     *     list IndustryDto 转换为 map IndustryDto
     * </pre>
     *
     * @param industryDtos 行业信息
     * @return map
     */
    private Map<String, IndustryDto> transferToIndustryDtoMap(List<IndustryDto> industryDtos) {
        if (industryDtos == null || industryDtos.isEmpty()) {
            return MapUtil.newHashMap();
        }
        String format = DateTypeEnum.YYYY_MM_DD.now();
        return industryDtos.stream().collect(Collectors.toMap(m -> m.getIndustryCode() + "_" + format, item -> item));
    }

    /**
     * <pre>
     *     list IndustryDto 转换为 map IndustryDto
     * </pre>
     *
     * @param industries 行业信息
     * @return map
     */
    private Map<String, Industry> transferToIndustryMap(List<Industry> industries) {
        if (industries == null || industries.isEmpty()) {
            return MapUtil.newHashMap();
        }
        String format = DateTypeEnum.YYYY_MM_DD.now();
        return industries.stream().collect(Collectors.toMap(m -> m.getIndustryCode() + "_" + format, item -> item));
    }


    /**
     * <pre>
     *     解析填充 Industry list
     * </pre>
     *
     * @param industries   数据库中的记录 （可能为空）
     * @param industryDtos 来自 sina 的记录
     * @return false 新增 true 保存
     */
    public TwoTuple<Boolean, List<IndustryDto>> parseIndustryList(List<Industry> industries, List<IndustryDto> industryDtos) {
        TwoTuple<Boolean, List<IndustryDto>> tuple = new TwoTuple<>();
        if (industries == null || industries.isEmpty()) {
            tuple.setFirst(false);
            tuple.setSecond(industryDtos);
            return tuple;
        }
        Map<String, IndustryDto> industryDtoMap = transferToIndustryDtoMap(industryDtos);
        Map<String, Industry> industryMap = transferToIndustryMap(industries);
        for (Map.Entry<String, Industry> entry : industryMap.entrySet()) {
            IndustryDto industryDto = industryDtoMap.get(entry.getKey());
            // 拷贝属性
            industryDto.copyProperties(entry.getValue());
        }
        tuple.setFirst(true);
        tuple.setSecond(industryDtos);
        return tuple;
    }

}
