package com.yafeed.trade.component;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @description: 新浪接口地址
 * @author: yanghj
 * @time: 2020-12-13 14:56
 */

@Component
@ConfigurationProperties(prefix = "sina.url")
@Getter
@Setter
public class SinaUrl {

    /**
     * 板块接口地址
     */
    private String industry;

    /**
     * 个股接口地址
     */
    private String individual;

    /**
     * 板块股票地址
     */
    private String industryStock;


}
