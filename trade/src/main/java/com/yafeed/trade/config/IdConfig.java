package com.yafeed.trade.config;

import com.robert.vesta.service.impl.IdServiceImpl;
import com.robert.vesta.service.impl.provider.IpConfigurableMachineIdProvider;
import com.robert.vesta.service.impl.provider.MachineIdProvider;
import com.robert.vesta.service.intf.IdService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @description: ID配置
 * @author: yanghj
 * @time: 2020-12-23 14:01
 **/
@Configuration
public class IdConfig {

    @Bean
    public IdService idService(MachineIdProvider machineIdProvider) {
        IdServiceImpl idService = new IdServiceImpl();
        idService.setMachineIdProvider(machineIdProvider);
        idService.init();
        return idService;
    }


    @Bean
    public MachineIdProvider machineIdProvider() {
        IpConfigurableMachineIdProvider ipConfigurableMachineIdProvider = new IpConfigurableMachineIdProvider();
        ipConfigurableMachineIdProvider.setIps("192.168.0.69");
        return ipConfigurableMachineIdProvider;
    }


}
