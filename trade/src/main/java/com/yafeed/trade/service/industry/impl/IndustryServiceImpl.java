package com.yafeed.trade.service.industry.impl;

import com.alibaba.fastjson.JSONObject;
import com.robert.vesta.service.intf.IdService;
import com.yafeed.trade.common.enums.DateTypeEnum;
import com.yafeed.trade.common.http.HttpUtils;
import com.yafeed.trade.common.tuple.TwoTuple;
import com.yafeed.trade.component.SinaUrl;
import com.yafeed.trade.dao.IndustryMapper;
import com.yafeed.trade.entity.industry.Industry;
import com.yafeed.trade.entity.industry.IndustryExample;
import com.yafeed.trade.entity.leaderstock.LeaderStock;
import com.yafeed.trade.pojo.bo.industry.IndustryBo;
import com.yafeed.trade.pojo.dto.industry.IndustryDto;
import com.yafeed.trade.service.industry.IndustryService;
import com.yafeed.trade.service.stock.IndustryStockService;
import com.yafeed.trade.service.stock.LeaderStockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @description: 板块信息
 * @author: yanghj
 * @time: 2020-12-13 14:47
 */

@Service
public class IndustryServiceImpl implements IndustryService {

    @Resource
    private IndustryMapper industryMapper;

    /**
     * 个股信息
     */
    private IndustryStockService industryStockService;


    /**
     * l领涨股
     */
    private LeaderStockService leaderStockService;

    /**
     * ID 服务
     */
    private IdService idService;

    /**
     * http 工具
     */
    private HttpUtils httpUtils;

    /**
     * 新浪接口地址
     */
    private SinaUrl sinaUrl;

    /**
     * <pre>
     *  查询最近添加的行业
     * </pre>
     *
     * @return Industry
     */
    @Override
    public Industry listLastIndustry() {
        return industryMapper.listLastIndustry();
    }

    /**
     * <pre>
     *     获取新浪板块信息
     * </pre>
     *
     * @return list
     */
    @Override
    public List<IndustryDto> fetchSinaIndustries() {
        String industryStr = httpUtils.get(sinaUrl.getIndustry(), String.class);
        String[] split = industryStr.split("=");
        JSONObject jsonObject = JSONObject.parseObject(split[1]);
        List<IndustryDto> industryDtos = new ArrayList<>();
        Set<Map.Entry<String, Object>> entries = jsonObject.entrySet();

        // 解析字符串
        for (Map.Entry<String, Object> entry : entries) {
            String serNo = entry.getValue().toString();
            IndustryDto industryDto = IndustryDto.of(serNo, idService);
            industryDtos.add(industryDto);
        }
        return industryDtos;
    }


    /**
     * <pre>
     *     保存行业信息
     * </pre>
     *
     * @return list
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public TwoTuple<Boolean, List<IndustryDto>> saveSinaIndustries() {
        Date date = DateTypeEnum.YYYY_MM_DD.date();
        IndustryExample industryExample = new IndustryExample();
        IndustryExample.Criteria criteria = industryExample.createCriteria();
        criteria.andCreateTimeGreaterThan(date);
        List<Industry> industries = industryMapper.selectByExample(industryExample);
        List<IndustryDto> industryDtos = fetchSinaIndustries();
        IndustryBo industryBo = IndustryBo.of();
        TwoTuple<Boolean, List<IndustryDto>> tuple = industryBo.parseIndustryList(industries, industryDtos);

        if (tuple.getFirst()) {
            // 更新
            updateSinaIndustries(industryDtos);
        } else {
            // 插入
            insertSinaIndustries(industryDtos);
        }
        return tuple;
    }

    /**
     * <pre>
     *     保存行业信息
     * </pre>
     *
     * @param industryDtos 行业信息
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void insertSinaIndustries(List<IndustryDto> industryDtos) {
        for (IndustryDto industryDto : industryDtos) {
            industryMapper.insertSelective(industryDto.toIndustry());
        }
    }


    /**
     * <pre>
     *     更新行信息
     * </pre>
     *
     * @param industryDtos 行业信息
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateSinaIndustries(List<IndustryDto> industryDtos) {
        if (industryDtos == null || industryDtos.isEmpty()) {
            return;
        }
        List<Industry> industries = industryDtos.stream().map(IndustryDto::toIndustry).collect(Collectors.toList());
        for (Industry industry : industries) {
            industryMapper.updateByPrimaryKeySelective(industry);
        }
    }


    /**
     * <pre>
     *     刷新新浪数据
     *     <p>
     *         1.保存板块信息
     *         2.保存领涨股信息
     *         3.保存行业个股信息
     *     </p>
     * </pre>
     *
     * @return boolean
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean refreshIndustryIndividualStockFromSina() {
        // 保存行业
        TwoTuple<Boolean, List<IndustryDto>> industryTuple = saveSinaIndustries();


        TwoTuple<Boolean, List<LeaderStock>> leaderTuple = IndustryBo.of().filterLeaderStock(industryTuple);

        leaderStockService.saveLeaderStock(leaderTuple);

        //更新行业个股
        industryStockService.fetchSinaIndustriesStock(industryTuple.getSecond());
        return true;
    }


    @Autowired
    public void setHttpUtils(HttpUtils httpUtils) {
        this.httpUtils = httpUtils;
    }

    @Autowired
    public void setSinaUrl(SinaUrl sinaUrl) {
        this.sinaUrl = sinaUrl;
    }

    @Autowired
    public void setIndustryStockService(IndustryStockService industryStockService) {
        this.industryStockService = industryStockService;
    }

    @Autowired
    public void setIdService(IdService idService) {
        this.idService = idService;
    }

    @Autowired
    public void setLeaderStockService(LeaderStockService leaderStockService) {
        this.leaderStockService = leaderStockService;
    }
}
