package com.yafeed.trade.service.stock;

import com.github.pagehelper.PageInfo;
import com.yafeed.trade.entity.historystock.HistoryStock;
import com.yafeed.trade.entity.industrystock.IndustryStock;

import java.util.List;

/**
 * @description: 历史个股
 * @author: yanghj
 * @time: 2020-12-18 16:09
 **/
public interface HistoryStockService {

    /**
     * <pre>
     *     获取个股记录
     * </pre>
     *
     * @param stockCode 股票代码
     * @param stockName 股票名称
     * @return list
     */
    List<HistoryStock> fetchSinaStockInfo(String stockCode, String stockName);


    /**
     * 插入股票记录
     *
     * @param historyStocks 历史个股信息
     * @return int
     */
    int insertHistoryStock(List<HistoryStock> historyStocks);


    /**
     * <pre>
     *     更新个股历史信息
     *     <p>
     *         1.从 industry_stock 中查询个股信息
     *         2.通过 com.yafeed.trade.service.stock.HistoryStockService#fetchSinaStockInfo(java.lang.String, java.lang.String) 获取个股
     *     </p>
     * </pre>
     *
     * @return int
     */
    int updateHistoryStock(List<IndustryStock> industryStocks);


    /**
     * <pre>
     *     修改历史数据
     * </pre>
     *
     * @param pageNo   页码
     * @param pageSize 大小
     * @return
     */
    PageInfo<IndustryStock> updateHistoryStock(int pageNo, int pageSize);
}
