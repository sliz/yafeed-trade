package com.yafeed.trade.service.stock.impl;

import com.alibaba.fastjson.JSONArray;
import com.robert.vesta.service.intf.IdService;
import com.yafeed.trade.common.enums.DateTypeEnum;
import com.yafeed.trade.common.http.HttpUtils;
import com.yafeed.trade.common.tuple.TwoTuple;
import com.yafeed.trade.component.SinaUrl;
import com.yafeed.trade.dao.IndustryStockMapper;
import com.yafeed.trade.entity.industrystock.IndustryStock;
import com.yafeed.trade.entity.industrystock.IndustryStockExample;
import com.yafeed.trade.pojo.bo.stock.IndustryStockBo;
import com.yafeed.trade.pojo.dto.industry.IndustryDto;
import com.yafeed.trade.service.stock.IndustryStockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @description: 个股相关操作
 * @author: yanghj
 * @time: 2020-12-13 23:01
 */

@Service
public class IndustryStockServiceImpl implements IndustryStockService {

    /**
     * 行业个股
     */
    @Resource
    private IndustryStockMapper industryStockMapper;

    /**
     * 新浪地址
     */
    private SinaUrl sinaUrl;

    /**
     * http 工具
     */
    private HttpUtils httpUtils;

    /**
     * ID 服务
     */
    private IdService idService;

    /**
     * @param page         页码
     * @param num          数量
     * @param industryCode 行业编码
     * @return list
     */
    @Override
    public List<IndustryStock> fetchSinaIndustryStock(int page, int num, String industryCode) {
        String industryStock = sinaUrl.getIndustryStock();
        String url = String.format(industryStock, page, num, industryCode);
        JSONArray objects = httpUtils.get(url, JSONArray.class);
        if (objects.isEmpty()) {
            return new ArrayList<>();
        }
        return objects.stream().map(item -> {
            Map<String, Object> map = (Map<String, Object>) item;
            return IndustryStockBo.or(map,idService);
        }).collect(Collectors.toList());
    }


    /**
     * <pre>
     *     获取行业个股信息
     *     <p>
     *         1.更具行业信息从获取行业个股
     *         2.存储入库
     *     </p>
     * </pre>
     *
     * @param industryDtos 行业信息
     * @return int
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Integer fetchSinaIndustriesStock(List<IndustryDto> industryDtos) {
        int sum = 0;
        List<IndustryStock> industryStockDtos = new ArrayList<>();

        for (IndustryDto industryDto : industryDtos) {
            int max = industryDto.getCompanies() / 80 + 1;
            for (int page = 1; page <= max; page++) {
                List<IndustryStock> temp = fetchSinaIndustryStock(page, 80, industryDto.getIndustryCode());
                industryStockDtos.addAll(temp);
            }
        }
        saveIndustryStock(industryStockDtos);
        return sum;
    }


    /**
     * <pre>
     *     新增行业下的个股
     * </pre>
     *
     * @param industryStocks 行业股票信息
     * @return int
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int insertIndustryStock(List<IndustryStock> industryStocks) {
        if (industryStocks.isEmpty()) {
            return 0;
        }
        for (IndustryStock stockDto : industryStocks) {
            industryStockMapper.insertSelective(stockDto);
        }
        return industryStocks.size();
    }


    /**
     * <pre>
     *     更新行业个股
     * </pre>
     *
     * @param industryStocks 行业个股信息
     * @return int
     */

    @Transactional(rollbackFor = Exception.class)
    @Override
    public int updateIndustryStock(List<IndustryStock> industryStocks) {

        if (industryStocks.isEmpty()) {
            return 0;
        }
        for (IndustryStock stockDto : industryStocks) {
            industryStockMapper.updateByPrimaryKeySelective(stockDto);
        }
        return industryStocks.size();
    }


    /**
     * <pre>
     *     保存行业个股
     * </pre>
     *
     * @param industryStocks 行业个股信息
     * @return list
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int saveIndustryStock(List<IndustryStock> industryStocks) {
        IndustryStockBo stockBo = IndustryStockBo.of();
        Date date = DateTypeEnum.YYYY_MM_DD.date();

        IndustryStockExample example = new IndustryStockExample();
        IndustryStockExample.Criteria criteria = example.createCriteria();
        criteria.andCreateTimeGreaterThan(date);
        List<IndustryStock> exists = industryStockMapper.selectByExample(example);
        TwoTuple<Boolean, List<IndustryStock>> twoTuple = stockBo.parseIndustryStock(exists, industryStocks);
        if (twoTuple.getFirst()) {
            updateIndustryStock(twoTuple.getSecond());
            return industryStocks.size();
        }
        return insertIndustryStock(twoTuple.getSecond());
    }

    /**
     * <pre>
     *      查询不同的个股标识
     * </pre>
     *
     * @return list
     */
    @Override
    public List<IndustryStock> listDistinctIndustryStockSymbol() {
        return industryStockMapper.listDistinctIndustryStockSymbol();
    }


    @Autowired
    public void setSinaUrl(SinaUrl sinaUrl) {
        this.sinaUrl = sinaUrl;
    }

    @Autowired
    public void setHttpUtils(HttpUtils httpUtils) {
        this.httpUtils = httpUtils;
    }


    @Autowired
    public void setIdService(IdService idService) {
        this.idService = idService;
    }
}
