package com.yafeed.trade.service.stock;

import com.yafeed.trade.entity.industrystock.IndustryStock;
import com.yafeed.trade.pojo.dto.industry.IndustryDto;

import java.util.List;

/**
 * @description: 个股信息
 * @author: yanghj
 * @time: 2020-12-13 22:28
 */
public interface IndustryStockService {

    /**
     * <pre>
     *     查询行业板块股票
     * </pre>
     *
     * @param page         页码
     * @param num          数量
     * @param industryCode 行业编码
     * @return list
     */
    List<IndustryStock> fetchSinaIndustryStock(int page, int num, String industryCode);


    /**
     * <pre>
     *     获取行业个股信息
     *     <p>
     *         1.更具行业信息从获取行业个股
     *         2.存储入库
     *     </p>
     * </pre>
     *
     * @param industryDtos 行业信息
     * @return int
     */
    Integer fetchSinaIndustriesStock(List<IndustryDto> industryDtos);


    /**
     * <pre>
     *     新增行业下的个股
     * </pre>
     *
     * @param industryStocks 行业股票信息
     * @return int
     */
    int insertIndustryStock(List<IndustryStock> industryStocks);


    /**
     * <pre>
     *     更新行业个股
     * </pre>
     *
     * @param industryStocks 行业个股信息
     * @return int
     */
    int updateIndustryStock(List<IndustryStock> industryStocks);


    /**
     * <pre>
     *     保存行业个股
     * </pre>
     *
     * @param industryStocks 行业个股信息
     * @return
     */
    int saveIndustryStock(List<IndustryStock> industryStocks);


    /**
     * <pre>
     *      查询不同的个股标识
     * </pre>
     *
     * @return list
     */
    List<IndustryStock> listDistinctIndustryStockSymbol();

}
