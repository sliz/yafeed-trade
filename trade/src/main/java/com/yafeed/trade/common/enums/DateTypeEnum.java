package com.yafeed.trade.common.enums;

import lombok.Getter;
import lombok.SneakyThrows;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * @description: 时间类型枚举
 * @author: yanghj
 * @time: 2020-12-16 11:27
 **/
@Getter
public enum DateTypeEnum {


    /**
     * yyyy-MM-dd HH:mm:ss
     */
    YYYY_MM_DD_HH_MM_SS("yyyy-MM-dd HH:mm:ss"),

    /**
     * yyyy-MM-dd
     */
    YYYY_MM_DD("yyyy-MM-dd");

    /**
     * 格式化风格
     */
    String pattern;


    DateTypeEnum(String format) {
        this.pattern = format;
    }

    /**
     * <pre>
     *     获取当前时间(字符串)
     * </pre>
     *
     * @return string
     */
    public String now() {
        Date date = Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant());
        return format(date);
    }

    /**
     * <pre>
     *     格式化时间
     * </pre>
     *
     * @param date Date
     * @return string
     */
    public String format(Date date) {
        SimpleDateFormat formatter = formatter();
        return formatter.format(date);
    }


    /**
     * <pre>
     *     获取当前时间(date)
     * </pre>
     *
     * @return date
     */
    @SneakyThrows
    public Date date() {
        SimpleDateFormat formatter = formatter();
        return formatter.parse(now());
    }

    private SimpleDateFormat formatter() {
        synchronized (this) {
            return new SimpleDateFormat(getPattern());
        }
    }

    /**
     * <pre>
     *     求两个时间差
     * </pre>
     *
     * @param date     减数
     * @param tradeDay 被减数
     * @return 时间差 (天)
     */
    public int subtractDay(Date date, Date tradeDay) {
        if (tradeDay.compareTo(date) < 0) {
            return 0;
        }
        long diff = date.getTime() - tradeDay.getTime();
        return (int) (diff / (1000 * 60 * 60 * 24));
    }

    /**
     * <pre>
     *     求两个时间差
     * </pre>
     *
     * @param date     减数
     * @param seconds 被减数
     * @return 时间差 (秒)
     */
    public int subtractSeconds(Date date, Date seconds) {
        if (seconds.compareTo(date) > 0) {
            return 0;
        }
        long diff = date.getTime() - seconds.getTime();
        return (int) (diff / 1000);
    }


}
