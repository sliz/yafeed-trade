package com.yafeed.trade.dao;

import com.yafeed.trade.entity.industry.Industry;
import com.yafeed.trade.entity.industry.IndustryExample;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
public interface IndustryMapper extends Mapper<Industry> {


    /**
     * <pre>
     *     查询最新添加的行业
     * </pre>
     *
     * @return 行业
     */
    Industry listLastIndustry();

//    long countByExample(IndustryExample example);
//
//    int deleteByExample(IndustryExample example);
//
//    List<Industry> selectByExample(IndustryExample example);
//
//    int updateByExampleSelective(@Param("record") Industry record, @Param("example") IndustryExample example);
//
//    int updateByExample(@Param("record") Industry record, @Param("example") IndustryExample example);
}