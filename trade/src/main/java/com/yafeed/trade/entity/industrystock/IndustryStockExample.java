package com.yafeed.trade.entity.industrystock;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class IndustryStockExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public IndustryStockExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIndustryStockIdIsNull() {
            addCriterion("industry_stock_id is null");
            return (Criteria) this;
        }

        public Criteria andIndustryStockIdIsNotNull() {
            addCriterion("industry_stock_id is not null");
            return (Criteria) this;
        }

        public Criteria andIndustryStockIdEqualTo(Long value) {
            addCriterion("industry_stock_id =", value, "industryStockId");
            return (Criteria) this;
        }

        public Criteria andIndustryStockIdNotEqualTo(Long value) {
            addCriterion("industry_stock_id <>", value, "industryStockId");
            return (Criteria) this;
        }

        public Criteria andIndustryStockIdGreaterThan(Long value) {
            addCriterion("industry_stock_id >", value, "industryStockId");
            return (Criteria) this;
        }

        public Criteria andIndustryStockIdGreaterThanOrEqualTo(Long value) {
            addCriterion("industry_stock_id >=", value, "industryStockId");
            return (Criteria) this;
        }

        public Criteria andIndustryStockIdLessThan(Long value) {
            addCriterion("industry_stock_id <", value, "industryStockId");
            return (Criteria) this;
        }

        public Criteria andIndustryStockIdLessThanOrEqualTo(Long value) {
            addCriterion("industry_stock_id <=", value, "industryStockId");
            return (Criteria) this;
        }

        public Criteria andIndustryStockIdIn(List<Long> values) {
            addCriterion("industry_stock_id in", values, "industryStockId");
            return (Criteria) this;
        }

        public Criteria andIndustryStockIdNotIn(List<Long> values) {
            addCriterion("industry_stock_id not in", values, "industryStockId");
            return (Criteria) this;
        }

        public Criteria andIndustryStockIdBetween(Long value1, Long value2) {
            addCriterion("industry_stock_id between", value1, value2, "industryStockId");
            return (Criteria) this;
        }

        public Criteria andIndustryStockIdNotBetween(Long value1, Long value2) {
            addCriterion("industry_stock_id not between", value1, value2, "industryStockId");
            return (Criteria) this;
        }

        public Criteria andStockCodeIsNull() {
            addCriterion("stock_code is null");
            return (Criteria) this;
        }

        public Criteria andStockCodeIsNotNull() {
            addCriterion("stock_code is not null");
            return (Criteria) this;
        }

        public Criteria andStockCodeEqualTo(String value) {
            addCriterion("stock_code =", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeNotEqualTo(String value) {
            addCriterion("stock_code <>", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeGreaterThan(String value) {
            addCriterion("stock_code >", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeGreaterThanOrEqualTo(String value) {
            addCriterion("stock_code >=", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeLessThan(String value) {
            addCriterion("stock_code <", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeLessThanOrEqualTo(String value) {
            addCriterion("stock_code <=", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeLike(String value) {
            addCriterion("stock_code like", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeNotLike(String value) {
            addCriterion("stock_code not like", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeIn(List<String> values) {
            addCriterion("stock_code in", values, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeNotIn(List<String> values) {
            addCriterion("stock_code not in", values, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeBetween(String value1, String value2) {
            addCriterion("stock_code between", value1, value2, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeNotBetween(String value1, String value2) {
            addCriterion("stock_code not between", value1, value2, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockNameIsNull() {
            addCriterion("stock_name is null");
            return (Criteria) this;
        }

        public Criteria andStockNameIsNotNull() {
            addCriterion("stock_name is not null");
            return (Criteria) this;
        }

        public Criteria andStockNameEqualTo(String value) {
            addCriterion("stock_name =", value, "stockName");
            return (Criteria) this;
        }

        public Criteria andStockNameNotEqualTo(String value) {
            addCriterion("stock_name <>", value, "stockName");
            return (Criteria) this;
        }

        public Criteria andStockNameGreaterThan(String value) {
            addCriterion("stock_name >", value, "stockName");
            return (Criteria) this;
        }

        public Criteria andStockNameGreaterThanOrEqualTo(String value) {
            addCriterion("stock_name >=", value, "stockName");
            return (Criteria) this;
        }

        public Criteria andStockNameLessThan(String value) {
            addCriterion("stock_name <", value, "stockName");
            return (Criteria) this;
        }

        public Criteria andStockNameLessThanOrEqualTo(String value) {
            addCriterion("stock_name <=", value, "stockName");
            return (Criteria) this;
        }

        public Criteria andStockNameLike(String value) {
            addCriterion("stock_name like", value, "stockName");
            return (Criteria) this;
        }

        public Criteria andStockNameNotLike(String value) {
            addCriterion("stock_name not like", value, "stockName");
            return (Criteria) this;
        }

        public Criteria andStockNameIn(List<String> values) {
            addCriterion("stock_name in", values, "stockName");
            return (Criteria) this;
        }

        public Criteria andStockNameNotIn(List<String> values) {
            addCriterion("stock_name not in", values, "stockName");
            return (Criteria) this;
        }

        public Criteria andStockNameBetween(String value1, String value2) {
            addCriterion("stock_name between", value1, value2, "stockName");
            return (Criteria) this;
        }

        public Criteria andStockNameNotBetween(String value1, String value2) {
            addCriterion("stock_name not between", value1, value2, "stockName");
            return (Criteria) this;
        }

        public Criteria andCurrentPriceIsNull() {
            addCriterion("current_price is null");
            return (Criteria) this;
        }

        public Criteria andCurrentPriceIsNotNull() {
            addCriterion("current_price is not null");
            return (Criteria) this;
        }

        public Criteria andCurrentPriceEqualTo(BigDecimal value) {
            addCriterion("current_price =", value, "currentPrice");
            return (Criteria) this;
        }

        public Criteria andCurrentPriceNotEqualTo(BigDecimal value) {
            addCriterion("current_price <>", value, "currentPrice");
            return (Criteria) this;
        }

        public Criteria andCurrentPriceGreaterThan(BigDecimal value) {
            addCriterion("current_price >", value, "currentPrice");
            return (Criteria) this;
        }

        public Criteria andCurrentPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("current_price >=", value, "currentPrice");
            return (Criteria) this;
        }

        public Criteria andCurrentPriceLessThan(BigDecimal value) {
            addCriterion("current_price <", value, "currentPrice");
            return (Criteria) this;
        }

        public Criteria andCurrentPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("current_price <=", value, "currentPrice");
            return (Criteria) this;
        }

        public Criteria andCurrentPriceIn(List<BigDecimal> values) {
            addCriterion("current_price in", values, "currentPrice");
            return (Criteria) this;
        }

        public Criteria andCurrentPriceNotIn(List<BigDecimal> values) {
            addCriterion("current_price not in", values, "currentPrice");
            return (Criteria) this;
        }

        public Criteria andCurrentPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("current_price between", value1, value2, "currentPrice");
            return (Criteria) this;
        }

        public Criteria andCurrentPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("current_price not between", value1, value2, "currentPrice");
            return (Criteria) this;
        }

        public Criteria andPrePriceIsNull() {
            addCriterion("pre_price is null");
            return (Criteria) this;
        }

        public Criteria andPrePriceIsNotNull() {
            addCriterion("pre_price is not null");
            return (Criteria) this;
        }

        public Criteria andPrePriceEqualTo(BigDecimal value) {
            addCriterion("pre_price =", value, "prePrice");
            return (Criteria) this;
        }

        public Criteria andPrePriceNotEqualTo(BigDecimal value) {
            addCriterion("pre_price <>", value, "prePrice");
            return (Criteria) this;
        }

        public Criteria andPrePriceGreaterThan(BigDecimal value) {
            addCriterion("pre_price >", value, "prePrice");
            return (Criteria) this;
        }

        public Criteria andPrePriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("pre_price >=", value, "prePrice");
            return (Criteria) this;
        }

        public Criteria andPrePriceLessThan(BigDecimal value) {
            addCriterion("pre_price <", value, "prePrice");
            return (Criteria) this;
        }

        public Criteria andPrePriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("pre_price <=", value, "prePrice");
            return (Criteria) this;
        }

        public Criteria andPrePriceIn(List<BigDecimal> values) {
            addCriterion("pre_price in", values, "prePrice");
            return (Criteria) this;
        }

        public Criteria andPrePriceNotIn(List<BigDecimal> values) {
            addCriterion("pre_price not in", values, "prePrice");
            return (Criteria) this;
        }

        public Criteria andPrePriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("pre_price between", value1, value2, "prePrice");
            return (Criteria) this;
        }

        public Criteria andPrePriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("pre_price not between", value1, value2, "prePrice");
            return (Criteria) this;
        }

        public Criteria andChangeAmountIsNull() {
            addCriterion("change_amount is null");
            return (Criteria) this;
        }

        public Criteria andChangeAmountIsNotNull() {
            addCriterion("change_amount is not null");
            return (Criteria) this;
        }

        public Criteria andChangeAmountEqualTo(BigDecimal value) {
            addCriterion("change_amount =", value, "changeAmount");
            return (Criteria) this;
        }

        public Criteria andChangeAmountNotEqualTo(BigDecimal value) {
            addCriterion("change_amount <>", value, "changeAmount");
            return (Criteria) this;
        }

        public Criteria andChangeAmountGreaterThan(BigDecimal value) {
            addCriterion("change_amount >", value, "changeAmount");
            return (Criteria) this;
        }

        public Criteria andChangeAmountGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("change_amount >=", value, "changeAmount");
            return (Criteria) this;
        }

        public Criteria andChangeAmountLessThan(BigDecimal value) {
            addCriterion("change_amount <", value, "changeAmount");
            return (Criteria) this;
        }

        public Criteria andChangeAmountLessThanOrEqualTo(BigDecimal value) {
            addCriterion("change_amount <=", value, "changeAmount");
            return (Criteria) this;
        }

        public Criteria andChangeAmountIn(List<BigDecimal> values) {
            addCriterion("change_amount in", values, "changeAmount");
            return (Criteria) this;
        }

        public Criteria andChangeAmountNotIn(List<BigDecimal> values) {
            addCriterion("change_amount not in", values, "changeAmount");
            return (Criteria) this;
        }

        public Criteria andChangeAmountBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("change_amount between", value1, value2, "changeAmount");
            return (Criteria) this;
        }

        public Criteria andChangeAmountNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("change_amount not between", value1, value2, "changeAmount");
            return (Criteria) this;
        }

        public Criteria andChangeRangeIsNull() {
            addCriterion("change_range is null");
            return (Criteria) this;
        }

        public Criteria andChangeRangeIsNotNull() {
            addCriterion("change_range is not null");
            return (Criteria) this;
        }

        public Criteria andChangeRangeEqualTo(BigDecimal value) {
            addCriterion("change_range =", value, "changeRange");
            return (Criteria) this;
        }

        public Criteria andChangeRangeNotEqualTo(BigDecimal value) {
            addCriterion("change_range <>", value, "changeRange");
            return (Criteria) this;
        }

        public Criteria andChangeRangeGreaterThan(BigDecimal value) {
            addCriterion("change_range >", value, "changeRange");
            return (Criteria) this;
        }

        public Criteria andChangeRangeGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("change_range >=", value, "changeRange");
            return (Criteria) this;
        }

        public Criteria andChangeRangeLessThan(BigDecimal value) {
            addCriterion("change_range <", value, "changeRange");
            return (Criteria) this;
        }

        public Criteria andChangeRangeLessThanOrEqualTo(BigDecimal value) {
            addCriterion("change_range <=", value, "changeRange");
            return (Criteria) this;
        }

        public Criteria andChangeRangeIn(List<BigDecimal> values) {
            addCriterion("change_range in", values, "changeRange");
            return (Criteria) this;
        }

        public Criteria andChangeRangeNotIn(List<BigDecimal> values) {
            addCriterion("change_range not in", values, "changeRange");
            return (Criteria) this;
        }

        public Criteria andChangeRangeBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("change_range between", value1, value2, "changeRange");
            return (Criteria) this;
        }

        public Criteria andChangeRangeNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("change_range not between", value1, value2, "changeRange");
            return (Criteria) this;
        }

        public Criteria andHighPriceIsNull() {
            addCriterion("high_price is null");
            return (Criteria) this;
        }

        public Criteria andHighPriceIsNotNull() {
            addCriterion("high_price is not null");
            return (Criteria) this;
        }

        public Criteria andHighPriceEqualTo(BigDecimal value) {
            addCriterion("high_price =", value, "highPrice");
            return (Criteria) this;
        }

        public Criteria andHighPriceNotEqualTo(BigDecimal value) {
            addCriterion("high_price <>", value, "highPrice");
            return (Criteria) this;
        }

        public Criteria andHighPriceGreaterThan(BigDecimal value) {
            addCriterion("high_price >", value, "highPrice");
            return (Criteria) this;
        }

        public Criteria andHighPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("high_price >=", value, "highPrice");
            return (Criteria) this;
        }

        public Criteria andHighPriceLessThan(BigDecimal value) {
            addCriterion("high_price <", value, "highPrice");
            return (Criteria) this;
        }

        public Criteria andHighPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("high_price <=", value, "highPrice");
            return (Criteria) this;
        }

        public Criteria andHighPriceIn(List<BigDecimal> values) {
            addCriterion("high_price in", values, "highPrice");
            return (Criteria) this;
        }

        public Criteria andHighPriceNotIn(List<BigDecimal> values) {
            addCriterion("high_price not in", values, "highPrice");
            return (Criteria) this;
        }

        public Criteria andHighPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("high_price between", value1, value2, "highPrice");
            return (Criteria) this;
        }

        public Criteria andHighPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("high_price not between", value1, value2, "highPrice");
            return (Criteria) this;
        }

        public Criteria andLowPriceIsNull() {
            addCriterion("low_price is null");
            return (Criteria) this;
        }

        public Criteria andLowPriceIsNotNull() {
            addCriterion("low_price is not null");
            return (Criteria) this;
        }

        public Criteria andLowPriceEqualTo(BigDecimal value) {
            addCriterion("low_price =", value, "lowPrice");
            return (Criteria) this;
        }

        public Criteria andLowPriceNotEqualTo(BigDecimal value) {
            addCriterion("low_price <>", value, "lowPrice");
            return (Criteria) this;
        }

        public Criteria andLowPriceGreaterThan(BigDecimal value) {
            addCriterion("low_price >", value, "lowPrice");
            return (Criteria) this;
        }

        public Criteria andLowPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("low_price >=", value, "lowPrice");
            return (Criteria) this;
        }

        public Criteria andLowPriceLessThan(BigDecimal value) {
            addCriterion("low_price <", value, "lowPrice");
            return (Criteria) this;
        }

        public Criteria andLowPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("low_price <=", value, "lowPrice");
            return (Criteria) this;
        }

        public Criteria andLowPriceIn(List<BigDecimal> values) {
            addCriterion("low_price in", values, "lowPrice");
            return (Criteria) this;
        }

        public Criteria andLowPriceNotIn(List<BigDecimal> values) {
            addCriterion("low_price not in", values, "lowPrice");
            return (Criteria) this;
        }

        public Criteria andLowPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("low_price between", value1, value2, "lowPrice");
            return (Criteria) this;
        }

        public Criteria andLowPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("low_price not between", value1, value2, "lowPrice");
            return (Criteria) this;
        }

        public Criteria andTradeVolumeIsNull() {
            addCriterion("trade_volume is null");
            return (Criteria) this;
        }

        public Criteria andTradeVolumeIsNotNull() {
            addCriterion("trade_volume is not null");
            return (Criteria) this;
        }

        public Criteria andTradeVolumeEqualTo(Long value) {
            addCriterion("trade_volume =", value, "tradeVolume");
            return (Criteria) this;
        }

        public Criteria andTradeVolumeNotEqualTo(Long value) {
            addCriterion("trade_volume <>", value, "tradeVolume");
            return (Criteria) this;
        }

        public Criteria andTradeVolumeGreaterThan(Long value) {
            addCriterion("trade_volume >", value, "tradeVolume");
            return (Criteria) this;
        }

        public Criteria andTradeVolumeGreaterThanOrEqualTo(Long value) {
            addCriterion("trade_volume >=", value, "tradeVolume");
            return (Criteria) this;
        }

        public Criteria andTradeVolumeLessThan(Long value) {
            addCriterion("trade_volume <", value, "tradeVolume");
            return (Criteria) this;
        }

        public Criteria andTradeVolumeLessThanOrEqualTo(Long value) {
            addCriterion("trade_volume <=", value, "tradeVolume");
            return (Criteria) this;
        }

        public Criteria andTradeVolumeIn(List<Long> values) {
            addCriterion("trade_volume in", values, "tradeVolume");
            return (Criteria) this;
        }

        public Criteria andTradeVolumeNotIn(List<Long> values) {
            addCriterion("trade_volume not in", values, "tradeVolume");
            return (Criteria) this;
        }

        public Criteria andTradeVolumeBetween(Long value1, Long value2) {
            addCriterion("trade_volume between", value1, value2, "tradeVolume");
            return (Criteria) this;
        }

        public Criteria andTradeVolumeNotBetween(Long value1, Long value2) {
            addCriterion("trade_volume not between", value1, value2, "tradeVolume");
            return (Criteria) this;
        }

        public Criteria andTradePriceIsNull() {
            addCriterion("trade_price is null");
            return (Criteria) this;
        }

        public Criteria andTradePriceIsNotNull() {
            addCriterion("trade_price is not null");
            return (Criteria) this;
        }

        public Criteria andTradePriceEqualTo(Long value) {
            addCriterion("trade_price =", value, "tradePrice");
            return (Criteria) this;
        }

        public Criteria andTradePriceNotEqualTo(Long value) {
            addCriterion("trade_price <>", value, "tradePrice");
            return (Criteria) this;
        }

        public Criteria andTradePriceGreaterThan(Long value) {
            addCriterion("trade_price >", value, "tradePrice");
            return (Criteria) this;
        }

        public Criteria andTradePriceGreaterThanOrEqualTo(Long value) {
            addCriterion("trade_price >=", value, "tradePrice");
            return (Criteria) this;
        }

        public Criteria andTradePriceLessThan(Long value) {
            addCriterion("trade_price <", value, "tradePrice");
            return (Criteria) this;
        }

        public Criteria andTradePriceLessThanOrEqualTo(Long value) {
            addCriterion("trade_price <=", value, "tradePrice");
            return (Criteria) this;
        }

        public Criteria andTradePriceIn(List<Long> values) {
            addCriterion("trade_price in", values, "tradePrice");
            return (Criteria) this;
        }

        public Criteria andTradePriceNotIn(List<Long> values) {
            addCriterion("trade_price not in", values, "tradePrice");
            return (Criteria) this;
        }

        public Criteria andTradePriceBetween(Long value1, Long value2) {
            addCriterion("trade_price between", value1, value2, "tradePrice");
            return (Criteria) this;
        }

        public Criteria andTradePriceNotBetween(Long value1, Long value2) {
            addCriterion("trade_price not between", value1, value2, "tradePrice");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}