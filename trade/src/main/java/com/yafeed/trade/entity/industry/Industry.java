package com.yafeed.trade.entity.industry;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

public class Industry implements Serializable {
    /**
     * 主键
     */
    @Id
    @Column(name = "industry_id")
    private Long industryId;

    /**
     * 行业名称
     */
    @Column(name = "industry_name")
    private String industryName;

    /**
     * 行业代码
     */
    @Column(name = "industry_code")
    private String industryCode;

    /**
     * 公司数量
     */
    private Integer companies;

    /**
     * 平均价格
     */
    @Column(name = "average_price")
    private BigDecimal averagePrice;

    /**
     * 涨跌额
     */
    @Column(name = "change_amount")
    private BigDecimal changeAmount;

    /**
     * 涨跌幅
     */
    @Column(name = "change_range")
    private BigDecimal changeRange;

    /**
     * 成交量
     */
    @Column(name = "trade_volume")
    private Long tradeVolume;

    /**
     * 成交额
     */
    @Column(name = "trade_price")
    private Long tradePrice;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    private static final long serialVersionUID = 1L;

    /**
     * 获取主键
     *
     * @return industry_id - 主键
     */
    public Long getIndustryId() {
        return industryId;
    }

    /**
     * 设置主键
     *
     * @param industryId 主键
     */
    public void setIndustryId(Long industryId) {
        this.industryId = industryId;
    }

    /**
     * 获取行业名称
     *
     * @return industry_name - 行业名称
     */
    public String getIndustryName() {
        return industryName;
    }

    /**
     * 设置行业名称
     *
     * @param industryName 行业名称
     */
    public void setIndustryName(String industryName) {
        this.industryName = industryName == null ? null : industryName.trim();
    }

    /**
     * 获取行业代码
     *
     * @return industry_code - 行业代码
     */
    public String getIndustryCode() {
        return industryCode;
    }

    /**
     * 设置行业代码
     *
     * @param industryCode 行业代码
     */
    public void setIndustryCode(String industryCode) {
        this.industryCode = industryCode == null ? null : industryCode.trim();
    }

    /**
     * 获取公司数量
     *
     * @return companies - 公司数量
     */
    public Integer getCompanies() {
        return companies;
    }

    /**
     * 设置公司数量
     *
     * @param companies 公司数量
     */
    public void setCompanies(Integer companies) {
        this.companies = companies;
    }

    /**
     * 获取平均价格
     *
     * @return average_price - 平均价格
     */
    public BigDecimal getAveragePrice() {
        return averagePrice;
    }

    /**
     * 设置平均价格
     *
     * @param averagePrice 平均价格
     */
    public void setAveragePrice(BigDecimal averagePrice) {
        this.averagePrice = averagePrice;
    }

    /**
     * 获取涨跌额
     *
     * @return change_amount - 涨跌额
     */
    public BigDecimal getChangeAmount() {
        return changeAmount;
    }

    /**
     * 设置涨跌额
     *
     * @param changeAmount 涨跌额
     */
    public void setChangeAmount(BigDecimal changeAmount) {
        this.changeAmount = changeAmount;
    }

    /**
     * 获取涨跌幅
     *
     * @return change_range - 涨跌幅
     */
    public BigDecimal getChangeRange() {
        return changeRange;
    }

    /**
     * 设置涨跌幅
     *
     * @param changeRange 涨跌幅
     */
    public void setChangeRange(BigDecimal changeRange) {
        this.changeRange = changeRange;
    }

    /**
     * 获取成交量
     *
     * @return trade_volume - 成交量
     */
    public Long getTradeVolume() {
        return tradeVolume;
    }

    /**
     * 设置成交量
     *
     * @param tradeVolume 成交量
     */
    public void setTradeVolume(Long tradeVolume) {
        this.tradeVolume = tradeVolume;
    }

    /**
     * 获取成交额
     *
     * @return trade_price - 成交额
     */
    public Long getTradePrice() {
        return tradePrice;
    }

    /**
     * 设置成交额
     *
     * @param tradePrice 成交额
     */
    public void setTradePrice(Long tradePrice) {
        this.tradePrice = tradePrice;
    }

    /**
     * 获取更新时间
     *
     * @return update_time - 更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间
     *
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}