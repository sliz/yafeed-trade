package com.yafeed.trade.entity.historystock;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "history_stock")
public class HistoryStock implements Serializable {
    /**
     * 主键
     */
    @Id
    @Column(name = "history_stock_id")
    private Long historyStockId;

    /**
     * 交易时间
     */
    @Column(name = "trade_day")
    private Date tradeDay;

    /**
     * 股票代码
     */
    @Column(name = "stock_code")
    private String stockCode;

    /**
     * 股票名称
     */
    @Column(name = "stock_name")
    private String stockName;

    /**
     * 开盘价
     */
    @Column(name = "open_price")
    private BigDecimal openPrice;

    /**
     * 收盘价
     */
    @Column(name = "close_price")
    private BigDecimal closePrice;

    /**
     * 最高价
     */
    @Column(name = "high_price")
    private BigDecimal highPrice;

    /**
     * 最低价
     */
    @Column(name = "low_price")
    private BigDecimal lowPrice;

    /**
     * 成交量
     */
    @Column(name = "trade_volume")
    private Long tradeVolume;

    /**
     * ma20 价
     */
    @Column(name = "ma20_price")
    private BigDecimal ma20Price = new BigDecimal(0);

    /**
     * ma20 成交量
     */
    @Column(name = "ma20_volume")
    private Long ma20Volume = 0L;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    private static final long serialVersionUID = 1L;

    /**
     * 获取主键
     *
     * @return history_stock_id - 主键
     */
    public Long getHistoryStockId() {
        return historyStockId;
    }

    /**
     * 设置主键
     *
     * @param historyStockId 主键
     */
    public void setHistoryStockId(Long historyStockId) {
        this.historyStockId = historyStockId;
    }

    /**
     * 获取交易时间
     *
     * @return trade_day - 交易时间
     */
    public Date getTradeDay() {
        return tradeDay;
    }

    /**
     * 设置交易时间
     *
     * @param tradeDay 交易时间
     */
    public void setTradeDay(Date tradeDay) {
        this.tradeDay = tradeDay;
    }

    /**
     * 获取股票代码
     *
     * @return stock_code - 股票代码
     */
    public String getStockCode() {
        return stockCode;
    }

    /**
     * 设置股票代码
     *
     * @param stockCode 股票代码
     */
    public void setStockCode(String stockCode) {
        this.stockCode = stockCode == null ? null : stockCode.trim();
    }

    /**
     * 获取股票名称
     *
     * @return stock_name - 股票名称
     */
    public String getStockName() {
        return stockName;
    }

    /**
     * 设置股票名称
     *
     * @param stockName 股票名称
     */
    public void setStockName(String stockName) {
        this.stockName = stockName == null ? null : stockName.trim();
    }

    /**
     * 获取开盘价
     *
     * @return open_price - 开盘价
     */
    public BigDecimal getOpenPrice() {
        return openPrice;
    }

    /**
     * 设置开盘价
     *
     * @param openPrice 开盘价
     */
    public void setOpenPrice(BigDecimal openPrice) {
        this.openPrice = openPrice;
    }

    /**
     * 获取收盘价
     *
     * @return close_price - 收盘价
     */
    public BigDecimal getClosePrice() {
        return closePrice;
    }

    /**
     * 设置收盘价
     *
     * @param closePrice 收盘价
     */
    public void setClosePrice(BigDecimal closePrice) {
        this.closePrice = closePrice;
    }

    /**
     * 获取最高价
     *
     * @return high_price - 最高价
     */
    public BigDecimal getHighPrice() {
        return highPrice;
    }

    /**
     * 设置最高价
     *
     * @param highPrice 最高价
     */
    public void setHighPrice(BigDecimal highPrice) {
        this.highPrice = highPrice;
    }

    /**
     * 获取最低价
     *
     * @return low_price - 最低价
     */
    public BigDecimal getLowPrice() {
        return lowPrice;
    }

    /**
     * 设置最低价
     *
     * @param lowPrice 最低价
     */
    public void setLowPrice(BigDecimal lowPrice) {
        this.lowPrice = lowPrice;
    }

    /**
     * 获取成交量
     *
     * @return trade_volume - 成交量
     */
    public Long getTradeVolume() {
        return tradeVolume;
    }

    /**
     * 设置成交量
     *
     * @param tradeVolume 成交量
     */
    public void setTradeVolume(Long tradeVolume) {
        this.tradeVolume = tradeVolume;
    }

    /**
     * 获取ma20 价
     *
     * @return ma20_price - ma20 价
     */
    public BigDecimal getMa20Price() {
        return ma20Price;
    }

    /**
     * 设置ma20 价
     *
     * @param ma20Price ma20 价
     */
    public void setMa20Price(BigDecimal ma20Price) {
        this.ma20Price = ma20Price;
    }

    /**
     * 获取ma20 成交量
     *
     * @return ma20_volume - ma20 成交量
     */
    public Long getMa20Volume() {
        return ma20Volume;
    }

    /**
     * 设置ma20 成交量
     *
     * @param ma20Volume ma20 成交量
     */
    public void setMa20Volume(Long ma20Volume) {
        this.ma20Volume = ma20Volume;
    }

    /**
     * 获取更新时间
     *
     * @return update_time - 更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间
     *
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}